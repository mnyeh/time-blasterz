﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CamFollow : MonoBehaviour
{
    // The target's transform we are following
    public Transform target;
    // The distance in the x-z plane to the target
    public float distance = 10.0f;
    // the height we want the camera to be above the target
    public float height = 8.0f;
    // How much the camera moves when lerping to our final location
    public float heightDamping = 2.0f;
    public float rotationDamping = 1.9f;

    private BoostManager _boostManager;

    void Start()
    {
        // Find the boost manager if there is one in this scene
        foreach( GameObject rootObject in gameObject.scene.GetRootGameObjects() )
        {
            BoostManager boostManager = rootObject.GetComponent<BoostManager>();
            if( boostManager != null )
            {
                _boostManager = boostManager;
                break;
            }
        }
    }

    void Update()
    {
        if( _boostManager == null )
        {
            return;
        }
        
        // Zoom the camera out during boost
        float targetDistance = (_boostManager.BoostEnabled)  ? 15.0f : 7.5f;
        if( distance != targetDistance )
        {
            distance = Mathf.Lerp( distance, targetDistance, Time.deltaTime );
        }
    }
	
	void LateUpdate()
    {
        // Early out if we don't have a target
        if( !target )
        {
            return;
        }

        // Calculate the current rotation angles
        float wantedRotationAngle = target.eulerAngles.y;
        float wantedHeight = target.position.y + height;

        float currentRotationAngle = transform.eulerAngles.y;
        float currentHeight = transform.position.y;

        // Damp the rotation around the y-axis
        currentRotationAngle = Mathf.LerpAngle( currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime );

        // Damp the height
        currentHeight = Mathf.Lerp( currentHeight, wantedHeight, heightDamping * Time.deltaTime );

        // Convert the angle into a rotation
        Quaternion currentRotation = Quaternion.Euler( 0, currentRotationAngle, 0 );

        // Set the position of the camera on the x-z plane to:
        // distance meters behind the target
        transform.position = target.position;
        transform.position -= currentRotation * Vector3.forward * distance;

        // Set the height of the camera
        transform.position = new Vector3( transform.position.x, currentHeight, transform.position.z );

        // Add camera shake if boost is enabled
        if( _boostManager != null && _boostManager.BoostEnabled )
        {
            transform.position += new Vector3( Random.Range( -0.05f, 0.05f ), Random.Range( -0.05f, 0.05f ), 0.0f );
        }

        // Always look at the target
        transform.LookAt( target );
    }
}
