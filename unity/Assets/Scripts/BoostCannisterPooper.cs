﻿using System.Collections.Generic;
using UnityEngine;

public class BoostCannisterPooper : MonoBehaviour
{
    [SerializeField]
    private GameObject _boostCannisterPrefab;
    
    [SerializeField]
    private Transform _target;

    [SerializeField]
    private GameObject _collidersParent;
    private List<Collider> _collidersToIgnore = new List<Collider>();
    
    [SerializeField]
    private float _poopRate;

    [SerializeField]
    private float _poopDelay;

    private float _initialTime;
    private float _lastFireTime;

    void Awake()
    {
        _initialTime = Time.time;
        _collidersToIgnore.AddRange( _collidersParent.GetComponentsInChildren<Collider>() );
    }
    
    void Update()
    {
        if( _target == null || ( Time.time - _initialTime ) < _poopDelay )
        {
            return;
        }
        
        // Fire a new rocket based on the fire rate
        if( Time.time > _lastFireTime + _poopRate )
        {
            // Instantiate and get boost cannister
            GameObject cannister = Instantiate( _boostCannisterPrefab, transform.position, Quaternion.identity );
            cannister.GetComponent<BoostCannisterBehavior>().TargetCollider = _target.GetComponentInChildren<MeshCollider>();

            foreach( Collider collider in _collidersToIgnore )
            {
                Physics.IgnoreCollision( collider, cannister.GetComponent<Collider>() );
            }
            
            float emitterYPos = transform.position.y; // Y distance from ground
            float Ay = Physics.gravity.y; // Ay (Acceleration Y)
            float targetXPos = _target.position.x - transform.position.x; //X distance from source
            float targetZPos = _target.position.z - transform.position.z; //Z distance from source

            //Calculate the X and Z distances from the target
            float Vix = targetXPos * (Mathf.Abs( Ay / ( 2.0f * emitterYPos) ) );
            float Viz = targetZPos * (Mathf.Abs( Ay / ( 2.0f * emitterYPos) ) );

            //Compare X to Z distances and use whichever is greater for the trajectory.  Add some y-axis velocity so it arcs
            float forceMultiplier = 1.3f;
            Vector3 targetVelocity = new Vector3( 0.0f, 5.0f, Mathf.Abs( Mathf.Abs( Vix ) < Mathf.Abs( Viz ) ? Vix : Viz ) * forceMultiplier );
            // If the chopper is on the other side of the car, flip the velocity direction
            if( _target.transform.InverseTransformPoint( transform.position ).x > 0.0f )
            {
                targetVelocity.z *= -1.0f;
            }
            cannister.GetComponent<Rigidbody>().velocity = transform.TransformDirection( targetVelocity );
            
            _lastFireTime = Time.time;
        }
    }
}
