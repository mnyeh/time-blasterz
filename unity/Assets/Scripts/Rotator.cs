﻿using UnityEngine;

public class Rotator : MonoBehaviour
{

    [SerializeField]
    private Vector3 _rotationVector;
    [SerializeField]
    private float _rate;
    
	void Update()
    {
        transform.Rotate( _rate * _rotationVector );
    }
}
