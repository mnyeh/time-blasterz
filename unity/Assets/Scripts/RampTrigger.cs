﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RampTrigger : MonoBehaviour
{
    [SerializeField]
    private SpeedUpUIBehavior _speedUpUI;

	[SerializeField]
    private Transform _playerCar;
	
	[SerializeField]
    private Transform _theChopper;
	
    [SerializeField]
    private GameObject _firstHitCutscene;

    [SerializeField]
    private GameObject _firstAudio;
	
	[SerializeField]
    private GameObject _secondAudio;
	
	[SerializeField]
    private GameObject _smokeGen;
	
	[SerializeField]
    private GameObject _sparkGen;

    [SerializeField]
    private GameObject _finalHitCutscene;

    [SerializeField]
    private DrivableCarController _target;

    [SerializeField]
    private Collider _targetCollider;

    [SerializeField]
    private float _targetSpeed;
	
	[SerializeField]
    private Rigidbody _carRigidBody;
	
	private static float _timesHit = 0;


    void Start()
    {
		GetComponent<Renderer>().enabled = false;
		if( _timesHit == 1 )
				{
					_firstAudio.SetActive( true );
					_smokeGen.SetActive( true );
				}
		if( _timesHit == 2 )
				{
					_secondAudio.SetActive( true );
					_smokeGen.SetActive( true );
					_sparkGen.SetActive( true );
				}
    }

    void OnTriggerEnter( Collider collider )
    {
        if( _targetCollider == collider )
        {
            if( _target.Speed >= _targetSpeed )
{
				_timesHit++; //Equivalent to _timesHit = _timesHit + 1
								
				if( _timesHit == 1 )
				{
					_playerCar.position = new Vector3(0.0f, 1.25f, 0.0f);
					_theChopper.position = new Vector3(0.0f, 6.07f, 55.0f);
					_playerCar.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
					_theChopper.eulerAngles = new Vector3(0.0f, 90.0f, 0.0f);
					_firstHitCutscene.SetActive( true );
					_firstAudio.SetActive( true );
					_smokeGen.SetActive( true );
				}
				else if( _timesHit == 2 )
				{
					_firstHitCutscene.SetActive( false );
					_playerCar.position = new Vector3(0.0f, 1.25f, 0.0f);
					_theChopper.position = new Vector3(0.0f, 6.07f, 55.0f);
					_playerCar.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
					_theChopper.eulerAngles = new Vector3(0.0f, 90.0f, 0.0f);
					_firstHitCutscene.SetActive( true );
					_secondAudio.SetActive( true );
					_smokeGen.SetActive( true );
					_sparkGen.SetActive( true );
				}
				else if( _timesHit == 3 )
				{
				_finalHitCutscene.SetActive( true );
				}
}
            else
            {
                _speedUpUI.OnShowSpeedUp();
			
            }
        }
    }
	
	void Update()
	{
		Debug.Log(_timesHit);
	}
}
