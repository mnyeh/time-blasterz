﻿using UnityEngine;

public class MusicKiller : MonoBehaviour
{

    [SerializeField]
    private GameObject _musicSource;


    void Awake()
    {
        _musicSource.SetActive( false );
    }

    
}
