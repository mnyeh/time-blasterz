﻿using UnityEngine;

public class CutsceneManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _fullCutscene;

    [SerializeField]
    private GameObject _shortCutscene;

    public void Awake()
    {
        if( gameObject.scene.name == "Level1" && GameManager.instance.PlayedLevel1Cutscene || 
            gameObject.scene.name == "Level2" && GameManager.instance.PlayedLevel2Cutscene )
        {
            _fullCutscene.SetActive( false );
            _shortCutscene.SetActive( true );
        }
    }
}
