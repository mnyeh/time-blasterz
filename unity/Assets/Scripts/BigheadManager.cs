﻿using System.Collections.Generic;
using UnityEngine;

public class BigheadManager : MonoBehaviour
{
    [SerializeField]
    private List<Transform> _bigheadTargets;
    
    [Header("JFK Head Collider Properties")]
    [SerializeField]
    private Transform _JFKTarget;

    [SerializeField]
    private Vector3 _JFKTargetScale;

    [SerializeField]
    private Vector3 _JFKTargetPosition;

    [Header( "Driver Sunglasses Properties" )]
    [SerializeField]
    private List<Transform> _sunglassesTargets;

    [SerializeField]
    private Vector3 _sunglassesTargetScale;
    
    [SerializeField]
    private Vector3 _sunglassesTargetPosition;

    [Header( "Sniper Override Properties" )]
    [SerializeField]
    private List<SniperBehavior> _snipers;

    [SerializeField]
    private float _trackingStrength;

    [SerializeField]
    private float _ticksUntilDead;

    void Awake()
    {
        if( !GameManager.instance.BigheadMode )
        {
            return;
        }

        Vector3 scale = new Vector3( 3.0f, 3.0f, 3.0f );
        foreach( Transform head in _bigheadTargets )
        {
            head.localScale = scale;
        }

        _JFKTarget.localScale = _JFKTargetScale;
        _JFKTarget.localPosition = _JFKTargetPosition;

        foreach( Transform sunglasses in _sunglassesTargets )
        {
            sunglasses.localScale = _sunglassesTargetScale;
            sunglasses.localPosition = _sunglassesTargetPosition;
        }

        foreach( SniperBehavior sniper in _snipers )
        {
            sniper.TrackingStrength = _trackingStrength;
            sniper.TicksUntilDead = _ticksUntilDead;
        }
    }
}
