﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pusher : MonoBehaviour
{

	
	[SerializeField]
    private Rigidbody _carRigidBody;
	
	[SerializeField]
    private int _pushForce;



    void FixedUpdate()
    {
		_carRigidBody.AddRelativeForce( Vector3.forward * _pushForce );
    }
	

}
