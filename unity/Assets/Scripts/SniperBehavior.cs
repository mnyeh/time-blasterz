﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperBehavior : MonoBehaviour {

    public enum SniperState
    {
        INACTIVE,
        IDLE,
        TRACKING
    }
    
    [SerializeField]
    private Collider _target;
    private DrivableCarController _car;

    [SerializeField]
    private GameObject _laser;
    private LineRenderer _laserLineRenderer;
    private LaserBehavior _laserBehavior;
    private Material _laserMaterial;

    private SniperState _state = SniperState.INACTIVE;
    private SniperState State
    {
        set
        {
            if( value == _state )
            {
                return;
            }

            if( value == SniperState.TRACKING )
            {
                _state = SniperState.TRACKING;
                _hits = 0;
                _laserMaterial.color = Color.green;

                _laserLineRenderer.enabled = true;
                _laserBehavior.enabled = true;
            }
            else if( value == SniperState.IDLE )
            {
                _state = SniperState.IDLE;
                _sweepCenter = transform.eulerAngles.y;
                _sweepCurrent = transform.eulerAngles;
                // Randomize sweep direction
                _sweepDirector = ( (int)( Random.value * 10 ) % 2 != 0 ) ? 1.0f : -1.0f;

                _laserLineRenderer.enabled = true;
                _laserBehavior.enabled = true;
            }
            else
            {
                _state = SniperState.INACTIVE;
                _laserLineRenderer.enabled = false;
                _laserBehavior.enabled = false;
            }
        }
    }

    [Header("Idle Settings")]
    [SerializeField]
    private float _idleDistance;
    [SerializeField]
    private float _sweepAngleRange;
    [SerializeField]
    private float _sweepStrength;
    private float _sweepCenter;
    private float _sweepDirector = 1.0f;
    private Vector3 _sweepCurrent;

    [Header("Tracking Settings")]
    [SerializeField]
    private float _trackingDistance;
    [SerializeField]
    private float _trackingStrength;
    public float TrackingStrength
    {
        set { _trackingStrength = value; }
    }
    [SerializeField]
    private float _ticksUntilDead;
    public float TicksUntilDead
    {
        set { _ticksUntilDead = value; }
    }
    private float _hits;
    
    void Start()
    {
        _laserLineRenderer = _laser.GetComponent<LineRenderer>();
        _laserBehavior = _laser.GetComponent<LaserBehavior>();
        _laserMaterial = _laser.GetComponent<Renderer>().material;

        if( _target != null )
        {
            _car = _target.GetComponentInParent<DrivableCarController>();
        }
    }
	
	void FixedUpdate()
    {
        // Figure out which state we should be in based on distance
        float sqrMag = ( _target.transform.position - transform.position ).sqrMagnitude;
        if( sqrMag < _trackingDistance * _trackingDistance )
        {
            State = SniperState.TRACKING;
        }
        else if( sqrMag < _idleDistance * _idleDistance )
        {
            State = SniperState.IDLE;
        }
        else
        {
            State = SniperState.INACTIVE;
        }

        // State behavior
        if( _state == SniperState.IDLE )
        {
            // Sweep back and forth based on angle range and sweep strength
            _sweepCurrent.y += _sweepDirector * _sweepStrength;
            transform.eulerAngles = _sweepCurrent;
            if( Mathf.Abs( _sweepCurrent.y - _sweepCenter) > _sweepAngleRange )
            {
                _sweepDirector *= -1.0f;
            }
        }
        else if( _state == SniperState.TRACKING )
        {
            // Lerp towards the target based on tracking strength
            Quaternion targetRotation = Quaternion.LookRotation( _target.transform.TransformPoint( 0.0f, 0.0f,  ( _car.Speed / _car.MaxSpeed ) * 0.5f ) - transform.position );
            float modifiedStr = Mathf.Min( _trackingStrength * Time.deltaTime, 1.0f );
            transform.rotation = Quaternion.Lerp( transform.rotation, targetRotation, modifiedStr );
            
            // Update the number of hits based on whether or not the laser hit the targe
            _hits = ( _laserBehavior.HitCollider == _target ) ? Mathf.Min( _hits + 1, _ticksUntilDead ) : Mathf.Max( _hits - 1, 0 );
            UpdateLaserColor();
            if( _hits >= _ticksUntilDead )
            {
                GameManager.instance.OnGameEndEventOccurred( gameObject );
            }
        }
	}

    private void UpdateLaserColor()
    {
        if( _hits == 0 )
        {
            _laserMaterial.color = Color.green;
            return;
        }

        // Move the color from green to yellow to red
        // 0.0:  R = 0.0, G = 1.0
        // 0.5:  R = 1.0, G = 1.0
        // 1.0:  R = 1.0, G = 0.0
        float ratio = _hits / ( 0.5f * _ticksUntilDead );
        float redRatio = Mathf.Clamp( Mathf.Min( ratio, 0.5f ) * 2.0f, 0.0f, 1.0f );
        float greenRatio = Mathf.Clamp( 1.0f - ( Mathf.Max( ratio, 0.5f ) * 2.0f - 1.0f ), 0.0f, 1.0f );
        _laserMaterial.color = new Color( redRatio, greenRatio, 0.0f );
    }
}
