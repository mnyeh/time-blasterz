﻿using UnityEngine;

public class ChoppaBehavior : MonoBehaviour {

    [SerializeField]
    private Transform _target;

    [SerializeField]
    private Vector3 _targetOffset;

    [Header("Figure 8 Values")]
    [SerializeField]
    private float _speed;

    [SerializeField]
    private float _xScale;

    [SerializeField]
    private float _yScale;
    
    private Vector3 _pivotOffset;
    private float _phase;
    private bool _invert = false;
    private const float TWO_PI = Mathf.PI * 2.0f;


    [Header("Rotation Values")]
    [SerializeField]
    private float angleOffset = 75.0f;
    [SerializeField]
    private float _radius = 300.0f;
    [SerializeField]
    private float _choppaChaseSpeed = 40.0f;
    [SerializeField]
    private Vector3 _origin = new Vector3( 260.1337f, 20.8f, 227.581f );
    private Vector3 _initialTargetLocation;

    // Only calculate the chopper's new position once per frame
    void FixedUpdate()
    {
        // Get the angle that the car is at
        Vector3 targetDir = _target.position - _origin;
        float targetAngle = Vector3.SignedAngle( targetDir, new Vector3( 0, 0, 1 ), Vector3.up );

        // Get the Choppa angle in radians offset by a little bit and calculate the position around the circle
        float angle = ( targetAngle + angleOffset ) * Mathf.Deg2Rad;
        _initialTargetLocation = _origin + new Vector3(Mathf.Cos( angle ) * _radius, 0.0f, Mathf.Sin( angle ) * _radius ) + _targetOffset;
    }

	void Update()
    {
        if( _target == null )
        {
            return;
        }

        // Move in a figure 8
        _pivotOffset = Vector3.up * 2.0f * _yScale;

        _phase += _speed * Time.deltaTime;
        if( _phase > TWO_PI )
        {
            _invert = !_invert;
            _phase -= TWO_PI;
        }

        if( _phase < 0.0f )
        {
            _phase += TWO_PI;
        }

        // Add the initial position + target offset + figure 8 movement
        Vector3 choppaFinalPosition = _initialTargetLocation + ( _invert ? _pivotOffset : Vector3.zero );
        choppaFinalPosition.x += Mathf.Sin( _phase ) * _xScale;
        choppaFinalPosition.y += Mathf.Cos( _phase ) * ( _invert ? -1.0f : 1.0f ) * _yScale;
        // MoveTowards in order to smooth movement
        transform.position = Vector3.MoveTowards( transform.position, choppaFinalPosition, Time.deltaTime * _choppaChaseSpeed );

        // Set rotation so the choppa is always facing the car
        transform.rotation = Quaternion.LookRotation( Quaternion.AngleAxis( -90.0f, Vector3.up ) * (_target.position - transform.position ) );
    }
}
