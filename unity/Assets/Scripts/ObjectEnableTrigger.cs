﻿using UnityEngine;

public class ObjectEnableTrigger : MonoBehaviour
{
    [SerializeField]
    private Collider _targetCollider;

    [SerializeField]
    private GameObject _target;

    void Start()
    {
        GetComponent<Renderer>().enabled = false;
    }

    void OnTriggerEnter( Collider collider )
    {
        if( _targetCollider == collider )
        {
            GetComponent<Collider>().enabled = false;
            _target.SetActive( true );
        }
    }
}
