﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrivableCarController : MonoBehaviour
{
    [SerializeField]
    private BoostManager _boostManager;

    [SerializeField]
    private List<AxleInfo> _axleInfos;

    [SerializeField]
    private float _maxMotorTorque;

    [SerializeField]
    private float _maxSteeringAngle;

    [SerializeField]
    private float _maxSpeed;
    public float MaxSpeed
    {
        get { return _maxSpeed; }
    }

    private Rigidbody _rigidBody;
    private const float DOWNFORCE_COEFFICIENT = -20.0f;

    // in KPH
    public float Speed
    {
        get { return _rigidBody.velocity.magnitude * 3.6f; }
    }

    public void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    public void ApplyLocalPositionToVisuals( WheelCollider collider )
    {
        if( collider.transform.childCount == 0 )
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose( out position, out rotation );

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    public void FixedUpdate()
    {
        float verticalAxis = Input.GetAxis("Vertical");
        float steering = _maxSteeringAngle * Input.GetAxis("Horizontal");
        float motor = _maxMotorTorque * verticalAxis;

        foreach( AxleInfo axleInfo in _axleInfos )
        {
            if( axleInfo.steering )
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if( axleInfo.motor )
            {
                Vector3 localVelocity = transform.InverseTransformDirection( _rigidBody.velocity );
                // If we're braking, increase the torque so it's more responsive
                if( localVelocity.z > 0.0f && verticalAxis < 0.0f )
                {
                    motor *= 1.4f;
                }
                // See if we're moving faster than max speed to cut the torque
                else if( Speed > _maxSpeed )
                {
                    motor = ( _maxSpeed - Speed ) * 10.0f;
                }

                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }

            ApplyLocalPositionToVisuals( axleInfo.leftWheel );
            ApplyLocalPositionToVisuals( axleInfo.rightWheel );
        }
            
        // Apply downforce
        _rigidBody.AddForceAtPosition( DOWNFORCE_COEFFICIENT * _rigidBody.velocity.sqrMagnitude * transform.up, transform.position );

        // Apply boost
        if( _boostManager != null && _boostManager.BoostEnabled )
        {
            _rigidBody.AddRelativeForce( Vector3.forward * _boostManager.BoostForce );
        }
    }
}

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor; // is this wheel attached to motor?
    public bool steering; // does this wheel apply steer angle?
}
