﻿using UnityEngine;
using UnityEngine.UI;

public class SpeedometerBehavior : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    [SerializeField]
    private DrivableCarController _car;
    
	void FixedUpdate()
    {
        _text.text = ( (int) _car.Speed ).ToString();
	}
}
