﻿using UnityEngine;

public class SpeedUpUIBehavior : MonoBehaviour
{
    [SerializeField]
    private float _lifetime;
    private float _initialTick;

    public void OnShowSpeedUp()
    {
        gameObject.SetActive( true );
        _initialTick = Time.time;
    }

    void Update()
    {
        if( Time.time - _initialTick > _lifetime )
        {
            gameObject.SetActive( false );
        }
    }

}
