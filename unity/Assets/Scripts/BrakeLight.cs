﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrakeLight : MonoBehaviour {

    private const string EmissiveValue = "_EmissionScaleUI";
    private const string EmissiveColor = "_EmissionColor";
    private Material _brakeMat;
    public Color _emissionColor = new Color(.7f, .08f, .08f);
    private Color _currentColor;

    private Light[] _brakeLights;
	// Use this for initialization
	void Start () {

        Renderer renderer = GetComponent<Renderer> ();
        _brakeMat = renderer.material;
        _brakeMat.EnableKeyword("_EMISSION");
        _currentColor = _brakeMat.GetColor("_EmissionColor");

        _brakeLights = GetComponentsInChildren<Light>();
	}
	
	// Update is called once per frame
    void Update () {
        float verticalAxis = Input.GetAxis("Vertical");
        bool braking = verticalAxis < 0;

        // Turn on brake lights if we're braking
        foreach(Light light in _brakeLights )
        {
            light.enabled = braking;
        }
        if( braking)
        {
            _brakeMat.SetFloat(EmissiveValue, 1.0f);
            _brakeMat.SetColor(EmissiveColor, _emissionColor);
        }
        else
        {
            _brakeMat.SetFloat(EmissiveValue, 0.0f);
            _brakeMat.SetColor(EmissiveColor, _currentColor);
        }
	}
}
