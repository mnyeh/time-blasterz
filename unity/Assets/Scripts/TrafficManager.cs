﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Utility;
using UnityStandardAssets.Vehicles.Car;

public class TrafficManager : MonoBehaviour {

    [SerializeField] private List<WaypointCircuit> _circuits; // A reference to the waypoint-based route we should follow
    public bool _testing = false;
    public float _wait = 3.0f;
    public float _waitRange = 2.0f;
    // List of prefabs to generate cars from
    public GameObject[] _carPrefabs;
    private List<GameObject> _cars;
    private GameObject _carEmpty;

    public int _maxCars = 25;
	// Use this for initialization
    void Start ()
    {
        _carEmpty = new GameObject();
        _carEmpty.transform.parent = transform;
        _cars = new List<GameObject>();
        SceneManager.activeSceneChanged += ActiveSceneChanged;
        if( _testing )
            SpawnCars();
    }

    private void ActiveSceneChanged(Scene prev, Scene current)
    {
        if( current.name == "Level1" )
        {
            SpawnCars();
        }
    }

    void SpawnCars()
    {
        foreach( WaypointCircuit pickedCircuit in _circuits)
        {
            int waypointLength = pickedCircuit.Waypoints.Length;
            int step = 6;
            int index = Random.Range(0, waypointLength - 1);
            while( _cars.Count < _maxCars )
            {
                GameObject pickedCarPrefab = _carPrefabs[Random.Range(0, _carPrefabs.Length)];

                index = (index + step) % waypointLength;

                Vector3 spawnLocation = pickedCircuit.Waypoints[index].position;
                Vector3 nextPosition  = pickedCircuit.Waypoints[(index + 1) % waypointLength].position;
                spawnLocation.y += .5f;
                nextPosition.y += .5f;

                // Create a new car prefab at the spawn location, and orient it towards the next waypoint
                GameObject me = Instantiate(pickedCarPrefab, spawnLocation, Quaternion.LookRotation( nextPosition - spawnLocation, pickedCarPrefab.transform.up));
                
                // Set the parent of the new car to this object
                me.transform.parent = _carEmpty.transform;

                // Set our circuit to the one we picked
                WaypointProgressTracker newProgressTracker = me.GetComponent<WaypointProgressTracker>();
                newProgressTracker.circuit = pickedCircuit;
                newProgressTracker.progressDistance = pickedCircuit.Distances[index];

                _cars.Add(me);
            }
        }
    }

    void OnDisable()
    {
        SceneManager.activeSceneChanged -= ActiveSceneChanged;
    }
}
