﻿using UnityEngine;

public class QuitScript : MonoBehaviour
{
    public void OnQuitPressed()
    {
        Application.Quit();
    }
}
