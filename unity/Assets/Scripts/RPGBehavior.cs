﻿using System.Collections.Generic;
using UnityEngine;

public class RPGBehavior : MonoBehaviour
{
    [SerializeField]
    private BoostManager _boostManager;

    [SerializeField]
    private GameObject _rocketPrefab;
    
    [SerializeField]
    private Transform _target;
    private DrivableCarController _car;
    private List<Collider> _targetColliders = new List<Collider>();

    [SerializeField]
    List<Collider> _collidersToIgnore;
    
    [SerializeField]
    private float _fireRate;

    [SerializeField]
    private float _fireDelay;

    [SerializeField]
    private float _rocketSpeed = 20.0f;

    [SerializeField]
    private float _maxTargetOffset;

    private float _lastFireTime;
    private float _initialTime;
    
    void Awake()
    {
        _initialTime = Time.time;
    }

    void Start()
    {
        if( _target != null )
        {
            _car = _target.GetComponent<DrivableCarController>();
            _targetColliders.AddRange( _car.GetComponentsInChildren<Collider>() );
        }
    }
    
	void Update()
    {
        if( _target == null || ( Time.time - _initialTime ) < _fireDelay )
        {
            return;
        }
        
        // Fire a new rocket based on the fire rate
        if( Time.time > _lastFireTime + _fireRate )
        {
            // If boost is enabled, don't lead the target so it's easier to dodge the rocket
            Vector3 targetPosition = _boostManager.BoostEnabled ? _target.position : _target.TransformPoint( 0.0f, 0.0f, ( ( _car.Speed / _car.MaxSpeed ) * _maxTargetOffset ) );

            // Instantiate and get rocket
            GameObject rocket = Instantiate( _rocketPrefab, transform.position, Quaternion.LookRotation( targetPosition - transform.position ) );
            RocketBehavior rocketBehavior = rocket.GetComponent<RocketBehavior>();
            rocketBehavior.TargetColliders = _targetColliders;
            rocketBehavior.Speed = _rocketSpeed;
            rocketBehavior.BigheadMode = GameManager.instance.BigheadMode;
            Collider rocketCollider = rocket.GetComponent<Collider>();

            // Ignore collision with certain objects so the rocket doesn't explode prematurely
            foreach( Collider collider in _collidersToIgnore )
            {
                Physics.IgnoreCollision( collider, rocketCollider );
            }
            _lastFireTime = Time.time;
        }
    }
}
