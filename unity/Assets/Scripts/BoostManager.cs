﻿using UnityEngine;
using UnityEngine.UI;

public class BoostManager : MonoBehaviour
{
    [SerializeField]
    private Image _boostBar;

    [SerializeField]
    private float _startingBoostAmount = 25.0f;

    [SerializeField]
    private float _boostPerCannister = 25.0f;

    [SerializeField]
    private float _drainPerTick = 0.4f;

    [SerializeField]
    private float _boostForce = 4.0f;
    public float BoostForce { get { return _boostForce; } }

    private float _boostAmount;
    private const float MAX_BOOST_AMOUNT = 100.0f;

    private bool _boostEnabled;
    public bool BoostEnabled
    {
        get { return _boostEnabled; }
    }

    public static BoostManager instance = null;
    
    void Awake()
    {
        if( instance == null )
        {
            instance = this;
        }
        else if( instance != this )
        {
            Destroy( gameObject );
        }
    }

    void Start()
    {
        _boostAmount = _startingBoostAmount;
        UpdateBoostBar();
    }

    void FixedUpdate()
    {
        if( Input.GetKey( KeyCode.J ) && Input.GetKey( KeyCode.F ) && Input.GetKey( KeyCode.K ) )
        {
            _boostAmount = MAX_BOOST_AMOUNT;
            UpdateBoostBar();
        }

        if( Input.GetKey( KeyCode.Space ) )
        {
            _boostAmount = Mathf.Max( _boostAmount - _drainPerTick, 0.0f );
            _boostEnabled = ( _boostAmount > 0.0f );

            UpdateBoostBar();
        }
        else
        {
            _boostEnabled = false;
        }
    }

    public void OnBoostCannisterHit()
    {
        _boostAmount = Mathf.Min( _boostAmount + _boostPerCannister, 100.0f );
        UpdateBoostBar();
    }

    private void UpdateBoostBar()
    {
        // Update scale
        _boostBar.transform.localScale = new Vector3( _boostAmount / MAX_BOOST_AMOUNT, _boostBar.transform.localScale.y, _boostBar.transform.localScale.z );

        // Update color from red to yellow to green
        // 0.0:  R = 1.0, G = 0.0
        // 0.5:  R = 1.0, G = 1.0
        // 1.0:  R = 0.0, G = 1.0
        float ratio = _boostAmount / MAX_BOOST_AMOUNT;
        float greenRatio = Mathf.Min( ratio, 0.5f ) * 2.0f;
        float redRatio = 1.0f - ( Mathf.Max( ratio, 0.5f ) * 2.0f - 1.0f );
        _boostBar.color = new Color( redRatio, greenRatio, 0.0f );
    }
}
