﻿using UnityEngine;

public class SceneTrigger : MonoBehaviour
{
    [SerializeField]
    private string _sceneToLoad;

	void Start()
    {
        GameManager.instance.OnSceneTriggerOccurred( gameObject.scene.name, _sceneToLoad );
    }
}
