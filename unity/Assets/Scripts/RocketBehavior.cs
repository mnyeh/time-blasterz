﻿using System.Collections.Generic;
using UnityEngine;

public class RocketBehavior : MonoBehaviour {

    [SerializeField]
    ParticleSystem _flameParticles;

    [SerializeField]
    ParticleSystem _explosionParticles;
    
	[SerializeField]
    GameObject _boomSound;
	
    [SerializeField]
    private float _secondsAlive = 5.0f;

    [SerializeField]
    private bool _bigheadMode;
    public bool BigheadMode
    {
        set { _bigheadMode = value; }
    }
    private bool _bigheadModeChecked;

    [SerializeField]
    private Transform _rocketHead;

    private bool moving = true;

    private List<Collider> _targetColliders;
    public List<Collider> TargetColliders
    {
        set { _targetColliders = value; }
    }

    private float _speed = 20.0f;
    public float Speed
    {
        set { _speed = value; }
    }

    void Awake()
    {
        Destroy( gameObject, _secondsAlive );
    }
    
	void Update()
    {
        if( !_bigheadModeChecked )
        {
            _bigheadModeChecked = true;
            if( _bigheadMode )
            {
                _rocketHead.localScale = new Vector3( 30.0f, 30.0f, 5.0f );
            }
        }

        if( moving )
        {
            transform.position += transform.forward * _speed * Time.deltaTime;
        }
	}

    void OnTriggerEnter( Collider collider )
    {
        moving = false;

        // Hide rocket
        foreach( MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>() )
        {
            renderer.enabled = false;
        }

        // Hide flame particles
        _flameParticles.Stop();

        // Play explosion
        _explosionParticles.Play();
		_boomSound.SetActive( true );

        // Check for game end event
        if( _targetColliders.Contains( collider ) )
        {
            GameManager.instance.OnGameEndEventOccurred( gameObject );
        }

        Destroy( gameObject, _explosionParticles.main.duration );
    }
}
