﻿using UnityEngine;

public class BoostCannisterBehavior : MonoBehaviour
{
    [SerializeField]
    private float _secondsAlive = 10.0f;

    private Collider _targetCollider;
    public Collider TargetCollider
    {
        set { _targetCollider = value; }
    }

    void Awake()
    {
        Destroy( gameObject, _secondsAlive );
    }
    
    void OnCollisionEnter( Collision collision )
    {
        if( _targetCollider == collision.collider )
        {
            BoostManager.instance.OnBoostCannisterHit();
            Destroy( gameObject );
        }
    }
}
