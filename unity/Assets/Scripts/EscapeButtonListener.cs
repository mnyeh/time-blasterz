﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeButtonListener : MonoBehaviour
{
    private Canvas _canvas;
     
    void Start()
    {
        _canvas = GetComponent<Canvas>();
    }

	void Update()
    {
		if( Input.GetKeyDown( KeyCode.Escape ) )
        {
            _canvas.enabled = !_canvas.enabled;
        }
	}
}
