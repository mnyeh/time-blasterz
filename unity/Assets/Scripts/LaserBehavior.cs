﻿using UnityEngine;

public class LaserBehavior : MonoBehaviour {

    private LineRenderer _lineRenderer;

    public Collider HitCollider;

	void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
	}
	
	void Update()
    {
        // Perform a raycast to see what got hit and stop drawing the laser at that point
        RaycastHit hit;
		if( Physics.Raycast( transform.position, transform.forward, out hit ) )
        {
            HitCollider = hit.collider;
            _lineRenderer.SetPosition( 1, transform.InverseTransformPoint( hit.point ) );
        }
        else
        {
            HitCollider = null;
        }
	}
}
