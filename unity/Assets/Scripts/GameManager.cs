﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

    [SerializeField]
    private GameObject _mainCamera;

    [SerializeField]
    private GameObject _mainMenu;

    private string _justLoadedScene = null;

    private bool _playedLevel1Cutscene = false;
    public bool PlayedLevel1Cutscene { get { return _playedLevel1Cutscene; } }
    private bool _playedLevel2Cutscene = false;
    public bool PlayedLevel2Cutscene { get { return _playedLevel2Cutscene; } }

    [SerializeField]
    private AudioSource _mainMenuAudio;

    [Header( "Bighead Mode Settings")]
    [SerializeField]
    private Toggle _bigheadToggle;

    [SerializeField]
    private List<Transform> _bigheadMainMenuTargets;

    [SerializeField]
    private Transform _sunglassesBigheadTarget;
    
    private bool _bigheadMode;
    public bool BigheadMode
    {
        get { return _bigheadMode; }
        set
        {
            _bigheadMode = value;
            // Special rules for the main screen since it needs to update when the button is hit
            Vector3 scale = value ? new Vector3( 3.0f, 3.0f, 3.0f ) : Vector3.one;
            Vector3 sunglassesPosition = value ? new Vector3( 0.052f, -0.058f, 0.26f ) : new Vector3( 0.05171011f, -0.2865408f, 0.05938726f );
            foreach( Transform head in _bigheadMainMenuTargets )
            {
                if( head != null )
                {
                    head.localScale = scale;
                }
            }

            if( _sunglassesBigheadTarget != null )
            {
                _sunglassesBigheadTarget.localScale = scale;
                _sunglassesBigheadTarget.localPosition = sunglassesPosition;
            }
        }
    }

    void Awake()
    {
        if( instance == null )
        {
            instance = this;
        }
        else if( instance != this )
        {
            Destroy( gameObject );
        }
    }

    public void OnMainMenuLoaded()
    {
        _bigheadToggle.isOn = false;
        _mainCamera.SetActive( true );
        _mainMenu.SetActive( true );

        _playedLevel1Cutscene = false;
        _playedLevel2Cutscene = false;

        List<GameObject> abortions = new List<GameObject>( gameObject.scene.GetRootGameObjects() );
        abortions.Remove( abortions.Find( ( obj ) => obj.name == "Main Scene Objects" ) );
        
        foreach( GameObject abortion in abortions )
        {
            Destroy( abortion );
        }

        _mainMenuAudio.Play();
    }

    public void OnGameEndEventOccurred( GameObject instantiator )
    {
        // Pause the game
        Time.timeScale = 0.0f;

        GameObject failScreen = null;
        foreach( GameObject gameObject in instantiator.scene.GetRootGameObjects() )
        {
            if( gameObject.name == "FailScreen" )
            {
                failScreen = gameObject;
                break;
            }
        }
        
        if( failScreen != null )
        {
            failScreen.SetActive( true );
        }
    }

    public void OnSceneTriggerOccurred( string from, string to )
    {
        if( from != null )
        {
            UnloadScene( from );
        }

        if( to != null )
        {
            LoadScene( to );
        }
    }
    
    public void LoadScene( string scene )
    {
        // Can't load main scene
        if( scene == "main" )
        {
            OnMainMenuLoaded();
            return;
        }

        SceneManager.LoadScene( scene, LoadSceneMode.Additive );
        _justLoadedScene = scene;
    }

    public void UnloadScene( string scene )
    {
        // Can't unload main scene
        if( scene == "main" )
        {
            return;
        }

        SceneManager.UnloadSceneAsync( scene );
    }

    void Update()
    {
        // Have to delay setting active because the scene is loaded on the next frame
        if( !string.IsNullOrEmpty( _justLoadedScene ) )
        {
            Scene scene = SceneManager.GetSceneByName( _justLoadedScene );
            if( scene.IsValid() && scene.isLoaded )
            {
                SceneManager.SetActiveScene( scene );
                Time.timeScale = 1.0f;
                if( scene.name == "Level1" )
                {
                    _playedLevel1Cutscene = true;
                }
                else if( scene.name == "Level2" )
                {
                    _playedLevel2Cutscene = true;
                }
                _justLoadedScene = null;
            }
        }
    }
}
