﻿using UnityEngine;

public class FailScreenBehavior : MonoBehaviour
{
	[SerializeField]
    private GameObject _musicSource;


    void Awake()
    {
        if( _musicSource != null )
        {
            _musicSource.SetActive( false );
        }
    }
	
    public void OnRetryLevelPressed()
    {
        GameManager.instance.UnloadScene( gameObject.scene.name );
        GameManager.instance.LoadScene( gameObject.scene.name );
    }

    public void OnQuitToMainMenuPressed()
    {
        GameManager.instance.UnloadScene( gameObject.scene.name );
        GameManager.instance.OnMainMenuLoaded();
    }
}
