﻿using UnityEngine;

public class IntroMoviePlayer : MonoBehaviour
{

    [SerializeField]
    private MovieTexture _videoChannel;

    private AudioSource _audioChannel;

    [SerializeField]
    private AudioSource _postIntroMovieAudio;

    void Start()
    {
        // Play the video
        _videoChannel.Play();
        // Grab the audio source applied in the editor and store it off, and play the audio
        _audioChannel = GetComponent<AudioSource>();
        _audioChannel.Play();
    }

    void Update()
    {
        // If any buttons are pressed, stop the video
        if( Input.anyKeyDown )
        {
            _videoChannel.Stop();
            _audioChannel.Stop();
        }

        // If the video is done playing, hide the video
        if( !_videoChannel.isPlaying )
        {
            gameObject.SetActive( false );
            _postIntroMovieAudio.Play();
        }
    }

    void OnGUI()
    {
        // Draw the actual video
        GUI.DrawTexture( new Rect( 0.0f, 0.0f, Screen.width, Screen.height ), _videoChannel );
    }
}
